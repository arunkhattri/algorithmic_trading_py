import pandas as pd
import matplotlib.pyplot as plt

# dummy stock prices
stock_prices = pd.DataFrame({'open': [36, 56, 45, 29, 65, 66, 67],
                             'close': [29, 72, 11, 4, 23, 68, 45],
                             'high': [42, 73, 61, 62, 73, 56, 55],
                             'low': [22, 11, 10, 2, 13, 24, 25]},
                            index=pd.date_range("2021-11-10",
                                                periods=7,
                                                freq="d"))

# plot
plt.figure()

# when close >= open
up = stock_prices[stock_prices.close >= stock_prices.open]
# when open > close
down = stock_prices[stock_prices.close < stock_prices.open]
# when up
clr_up = 'blue'
# when down
clr_down = 'red'

# width of candlestick elements
width = .1
widthh = .1

# plotting up prices
plt.bar(up.index, up.high - up.low, width, bottom=up.low, color=clr_up)
plt.barh(up.open, width=1, height=widthh, left=up.open + widthh, color=clr_up)

# plotting down prices
plt.bar(down.index, down.high - down.low, width, bottom=down.low, color=clr_down)

plt.xticks(rotation=30, ha='right')
plt.show()
