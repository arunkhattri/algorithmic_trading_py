import pandas as pd
import matplotlib.pyplot as plt

# dummy stock prices
stock_prices = pd.DataFrame({'open': [36, 56, 45, 29, 65, 66, 67],
                             'close': [29, 72, 11, 4, 23, 68, 45],
                             'high': [42, 73, 61, 62, 73, 56, 55],
                             'low': [22, 11, 10, 2, 13, 24, 25]},
                            index=pd.date_range("2021-11-10",
                                                periods=7,
                                                freq="d"))

# plot
plt.figure()

# when close >= open
up = stock_prices[stock_prices.close >= stock_prices.open]
# when open > close
down = stock_prices[stock_prices.close < stock_prices.open]
# when up
clr_up = 'blue'
# when down
clr_down = 'red'

# width of candlestick elements
width = .3
width2 = .03

# plotting up prices
plt.bar(up.index, up.close - up.open, width, bottom=up.open, color=clr_up)
plt.bar(up.index, up.high - up.close, width2, bottom=up.close, color=clr_up)
plt.bar(up.index, up.low - up.open, width2, bottom=up.open, color=clr_up)

# plotting down prices
plt.bar(down.index, down.close - down.open, width, bottom=down.open, color=clr_down)
plt.bar(down.index, down.high - down.open, width2, bottom=down.open, color=clr_down)
plt.bar(down.index, down.low - down.close, width2, bottom=down.close, color=clr_down)

plt.xticks(rotation=30, ha='right')
plt.show()
